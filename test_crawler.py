from crawl import Spider
import unittest
from network import Network
import os

class Spider_test_case(unittest.TestCase):
    def assert_do_web_page_exist(self , crawler , expected_output):
        data = crawler.db.execute_query('select status from links_html')
        data = data.fetchall()
        self.assertEqual(data , expected_output)
        
    def assert_all_links_added_to_db(self , crawler , expected_links):
        data = crawler.db.execute_query('select url from links_html')
        data = data.fetchall()
        self.assertEqual(set(data) , expected_links)
        
class Test_spider(Spider_test_case):
    def setUp(self):
        self.crawler = Spider('http://localhost:8000/' , 'http://localhost:8000/index.html' , 'localhost')
    
    def tearDown(self):
        os.remove(os.getcwd() , 'xyz.db')
        
    def test_links_added_to_db(self):
        all_links = [
                     ('http://localhost:8000/index.html',),
                     ('http://localhost:8000/1.html',),
                     ('http://localhost:8000/2.html',),
                     ('http://localhost:8000/3.html',),
                     ('http://localhost:8000/4.html',),
                     ('http://localhost:8000/5.html',),
                     ('http://localhost:8000/6.html',)
                     ]
        expected_links = set(all_links)
        self.crawler.add_links_to_db()
        self.assert_all_links_added_to_db(self.crawler , expected_links)
    
    def test_crawl_and_store(self):
        expected_output = [
                ('scraped' ,),
                ('scraped' ,),
                ('scraped' ,),
                ('scraped' ,),
                ('scraped' ,),
                ('scraped' ,),
                ('scraped' ,)
                ]
        self.crawler.crawl_and_store()
        self.assert_do_web_page_exist(self.crawler , expected_output)


class Network_test_case(unittest.TestCase):
    def assert_all_link_connections_present(self , network , expected_output):
        data = network.conn.execute('select * from network')
        data = data.fetchall()
        self.assertEqual(set(data) , expected_output)
        
    def assert_shortest_path(self , network , expected_path , src ,dest):
        path_found = network.find_shortest_path(src , dest)
        self.assertEqual(path_found , expected_path)
    
class Test_network(Network_test_case):
    def setUp(self):
        self.network = Network('http://localhost:8000/' , 'http://localhost:8000/index.html' , 'localhost')
        self.network.initiation_function()
    
    def tearDown(self):
        os.remove(os.getcwd() , 'network.db')
        
    def test_find_and_add_links(self):
        pathes = [
                ('http://localhost:8000/index.html', 'http://localhost:8000/2.html'),
                ('http://localhost:8000/index.html', 'http://localhost:8000/1.html'),
                ('http://localhost:8000/2.html', 'http://localhost:8000/4.html'),
                ('http://localhost:8000/2.html', 'http://localhost:8000/3.html'),
                ('http://localhost:8000/1.html', 'http://localhost:8000/2.html'),
                ('http://localhost:8000/1.html', 'http://localhost:8000/3.html'),
                ('http://localhost:8000/4.html', 'http://localhost:8000/6.html'),
                ('http://localhost:8000/4.html', 'http://localhost:8000/5.html'),
                ('http://localhost:8000/3.html', 'http://localhost:8000/4.html'),
                ('http://localhost:8000/3.html', 'http://localhost:8000/5.html'),
                ('http://localhost:8000/6.html', 'http://localhost:8000/index.html'),
                ('http://localhost:8000/6.html', 'http://localhost:8000/5.html'),
                ('http://localhost:8000/5.html', 'http://localhost:8000/1.html'),
                ('http://localhost:8000/5.html', 'http://localhost:8000/4.html')]
        expected_pathes = set(pathes)
        self.assert_all_link_connections_present(self.network , expected_pathes)
        
    def test_shortest_path(self):
        src = 'http://localhost:8000/index.html'
        dest = 'http://localhost:8000/4.html'
        expected_shortest_path = ['http://localhost:8000/index.html', 'http://localhost:8000/2.html', 'http://localhost:8000/4.html']
        self.assert_shortest_path(self.network , expected_shortest_path , src , dest)

if __name__ == '__main__':
    unittest.main()
        
    