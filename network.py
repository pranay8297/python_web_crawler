from urllib.request import urlopen
from urllib.parse import urlparse
from link_finder import LinkFinder
import sqlite3
import networkx as nx

class Network:
    def __init__(self , base_url , start_url , domain_name):
        self.base_url = base_url
        self.start_url = start_url
        self.domain_name = domain_name
        self.conn = self.create_db()
        self.crawled = set()
        self.graph = self.create_digraph()
        
    def execute_query(self , command):
        data = self.conn.execute(command)
        return data
        
    def gather_links(self , start_url):
        html_string = ''
        try:
            response = urlopen(start_url)
            if 'text/html' in response.getheader('Content-Type'):
                html_bytes = response.read()
                html_string = html_bytes.decode("utf-8")
            finder = LinkFinder(self.base_url, start_url)
            finder.feed(html_string)
        except Exception as e:
            print(str(e))
            return set()
        return finder.page_links()
    
    def start_process(self):
        self.insert_links_to_db(self.start_url)
        self.crawled.add(self.start_url)
        return True
    
    
    def create_db(self):
        self.conn = sqlite3.connect('net.db')
        self.conn.execute("create table network (src_url TEXT , dest_url TEXT)")
        self.conn.commit()
        return self.conn
    
    def insert_link(self, src_url , dest_url):
        self.conn.execute("insert into network (src_url , dest_url) values(? , ?)", (src_url , dest_url))
        self.conn.commit()
        return True
    
    def insert_links_to_db(self , src_url):
        links = self.gather_links(src_url)
        for dest_url in links:
            self.insert_link(src_url , dest_url)
        self.crawled.add(src_url)
        return True
    
    def find_number_of_links(self):
        count = list(self.conn.execute('select count(*) from network'))[0][0]
        return count
    
    def get_domain_name(self , url):
        try:
            domain = urlparse(url).netloc.split('.')
            if len(domain) < 2:
                return domain[-1]
            else:
                return domain.split('.')[-2] + '.' + domain.split('.')[-1]
        except:
            return ''
    
    def get_link(self):
        data = self.conn.execute("select dest_url from network")
        for link in data:
            x = link[0]
            if x not in self.crawled:
                domain_x = self.get_domain_name(x)
                if self.domain_name not in domain_x:
                    pass
                else:
                    return x
    
    def find_and_add_links(self):
        l = self.find_number_of_links()
        while l < 14:
            link = self.get_link()
            self.insert_links_to_db(link)
            l = self.find_number_of_links()
        return True
    
    def create_digraph(self):
        g = nx.DiGraph()
        return g
    
    def add_links_to_list_of_tuples(self):
        graph_links = []
        data = self.conn.execute("select * from network")
        for tup in data:
            graph_links.append(tup)
        return graph_links
            
    def add_edges_to_graph(self , list_of_tups):
        for tup in list_of_tups:
            self.graph.add_edge(tup[0] , tup[1])  
        return True
    
    def find_shortest_path(self , src , dest):
        shortest_path = nx.shortest_path(self.graph, src, dest)
        return shortest_path
    
    def initiation_function(self):
        self.start_process()
        self.find_and_add_links()
        list_of_tuples = self.add_links_to_list_of_tuples()
        self.add_edges_to_graph(list_of_tuples) 
        return True
    
    def delete_table(self):
        self.conn.execute('DROP TABLE network')

if __name__ == '__main__':
    obj = Network('http://localhost:8000/' , 'http://localhost:8000/index.html' , 'localhost')
    obj.initiation_function()
    print(obj.find_shortest_path('http://localhost:8000/index.html' , 'http://localhost:8000/4.html'))
    obj.delete_table()

