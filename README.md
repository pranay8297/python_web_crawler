WEB CRAWLER

step 1: create an object of class Spider from crawl.py with 'base_url' , 'start_url' , 'domain_name'

step 2: to gather all the links and html files of those links run "object_name.final_function()", this 
link gathers all the links and html bodies of the links and stores in the database('wxyz.db')

__________________________________________________________

NETWORKX PART

step 1: create an object of network class from 'networ.py' and pass base_url , start_url and domain_name of the website

step 2: after creating the object run 'object_name.initiation_function()' to initiate the object and store all the source
and destination links int the database

step 3: default number of rows are around 4000, you can change it if you want

step $: find the shortest path of two links by using "object_name.find_shortest_path(src_url , dest_url)" 

__________________________________________________________

TESTING PART
step 1: Open Terminal and run the following command
    -> python3 -m unittest test_crawler.py
    -> The terminal will display testcase results weather it is successfull or not.

To know the coverage of the testcases and the program
-> open terminal and run the following commands
-> pip install coverage
-> coverage run tests.py
-> coverage report
-> coverage html
-> open htmlcov/index.html
The last command opens a HTML webpage and shows the coverage of each perticular file


